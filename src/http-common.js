import axios from "axios";

const instance =  axios.create({
    baseURL: "http://0.0.0.0:5000",
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        "Content-type": "application/json; charset=UTF-8"
    }
});

export default instance;