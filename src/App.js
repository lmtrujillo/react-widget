import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';
import TableBody from '@material-ui/core/TableBody';
import Link from '@material-ui/core/Link';
import CardActions from '@material-ui/core/CardActions'
import dataService from './dataService'
import './App.css'

class App extends Component {

    state = {
      loading: true,
      matchesList: [],
    }

    getData = this.getData.bind(this);

  componentDidMount() {
    this.getData();
    //refresh every minute
    setInterval(this.getData, 60000);
  }

  async getData(){
    const response = await dataService.getAllLiveScores();
    const data = await response;
    this.setState({matchesList: data.data.data.match, loading: false})
  }


  render() {
    return (
      <div>
        {this.state.loading || !this.state.matchesList ? (<div>loading..</div>): (
      <Card className="main-card">
      <TableContainer className="main-table">
      <Table stickyHeader>
        <TableHead>
          <TableRow>
            <TableCell className="title-cell tb-cell">Home Team</TableCell>
            <TableCell className="title-cell tb-cell">Score</TableCell>
            <TableCell className="title-cell tb-cell">Away Team</TableCell>
            <TableCell className="title-cell tb-cell">Scheduled</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.state.matchesList.map((row) => (
            <TableRow  key={row.id}>
              <TableCell className="tb-cell">{row.home_name}</TableCell>
              <TableCell className="tb-cell">{row.score}</TableCell>
              <TableCell className="tb-cell">{row.away_name}</TableCell>
              <TableCell className="tb-cell">{row.scheduled}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      </TableContainer>
      <CardActions className="card-bottom">
      <Link className="bottom-font" href="https://livescore-api.com/">
          Widget designed by: Live-Score API
      </Link>
      </CardActions>
      </Card>
      )}
      </ div>

    )
  }

}

export default App;